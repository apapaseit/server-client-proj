### Contributing to the Server Client Project

This document outlines some basic guidelines for contributing to the
Server/Client project for the University of Liverpool end-module
programming assignment.

To contribute to this project, we use merge requests and branches. As stated
in the `README.md` any pushes directly to the `main` branch will be reverted.

# Clone from Source

You can clone from GitLab with:

```bash
git clone git@gitlab.csc.liv.ac.uk:sgasettl/server-client-proj.git
```

Or:

```bash
git clone https://gitlab.csc.liv.ac.uk/sgasettl/server-client-proj.git
```

# Using Branches

1. Change the current working directory to the location where you cloned
   the GitLab repository.

```bash
$ cd <REPOSITORY NAME>
```

2. Create a branch for changes.

```bash
$ git checkout -b NAME-OF-BRANCH
```

In the new branch, you can now make changes to existing files and add new files
as needed.

3. Commit your changes. Ensure your  commit messages have a maximum of 72
   characters with an exact summary. For example:

  ```bash
  doc: fixing contributing file typo
  ```

4. Create a merge request (PR) to the upstream repo for your branch.

   If this PR is related to an outstanding GitLab issue ensure you
   include a link to that GitLab issue in the comment.

# Reviewing Merge Requests

Each merge request requires at least one review and approval from an
active contributor. The primary function of reviews is to ensure
quality. However, we want merge requests to be viewed quickly to avoid code
contributors being blocked.

We ask that reviewers perform a basic sanity check on all code and
documentation contributions. Consider the following aspects when performing
a review:

- Does this make sense?
- Does this code perform what the code contributor has described in the commit message?
- Is this PEP8 compliant?
- Are the unittest's running successfully?
- Does this code require documentation? If so, does it have documentation in the commit?
- Are the comments (if there are any) clear and useful?

# Merging Merge Requests

Before merging, check the following:

- Ensure you can run tests locally successfully
- At least one reviewer from the team has approved the merge request.
- There is no outstanding change requests from other reviewers.

If all of these are complete, you can merge your own merge request. Please
do not merge a merge request that is not your own unless this action has been
previously discussed with the merge request author.

## Submitting Bugs

If you spot any issues or concerns with the code, please submit all issues
via GitLab in [Issues](https://gitlab.csc.liv.ac.uk/sgasettl/server-client-proj/-/issues).

## Document your Changes

If you have modified anything, please ensure that the code is appropriately
commented for the team's understanding. If you change the way the code is run,
ensure your merge request is submitted with the relevant documentation changes.
