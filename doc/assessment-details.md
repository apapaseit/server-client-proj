## End of Module Practical assessment

For this summative assignment, build a simple client/server network. Once the network is established, please complete the following tasks:

Create a dictionary, populate it, serialize it and send it to a server.
Create a text file and send it to a server.
With the dictionary, the user should be able to set the pickling format to one of the following: binary, JSON and XML. Also, the user will need to have the option to encrypt the text in a text file.

The server should have a configurable option to print the contents of the sent items to the screen and or to a file. Also, the server will need to be able to handle encrypted contents.

The client and server can be on separate machines or on the same machine.

Make sure that the code is written to PEP standard and uses exception handling to handle potential errors. Write unit tests. Upload the project to your source control website. Make sure that the commits have messages that describe the changes. Make sure the code is documented.

Members of the group should provide a code review to each other's code. The team should assign the following roles to the team members. If you have less than four members, then team members can have more than one role.

1. Project Manager: manages the project
2. Software Architect: designs the solution
3. Software Engineer: writes the code
4. Tester: tests the code

You will need to submit your code and a report of between 1000 and 1500 words. Your code submission should include your directory tree as well as code documentation. This should include as a minimum a Readme.md and dependencies.txt as well as other documentation that you may see fit to include. In addition, the log of Github/GitLab push comments and code reviews should be included.
