% TDOH Version 1.0 | Command-line options documentation

NAME
====

Server/Client Network

SYNOPSIS
========

| \[help]\[new-dictionary]\[add-entry "key" "value"]\[serialize-dictionary **pickling-format**]
  \[display-dictionary] \[encrypt **file_name**] \[send-file **file_name**] \[exit]

DESCRIPTION
===========

This is a basic implementation of a client and server network.

The client creates a dictionary, populates it, serializes it, and
sends it to the server.

Once started and upon a successful connection to the server, the client
will listen to commands from the user.

Options
-------

help

:    this command will display on the screen all the features/commands
     available in the client.

new-dictionary

:   Creates a new dictionary object

add-entry "key" "value"

:   Adds a new entry to an existing dictionary. Automatically creates a new
    dictionary object if none exists

serialize-dictionary [pickling-format]

:   Dumps the existing dictionary into a file. The pickling format can be
    BINARY, JSON, or XML. The default value is JSON.

display-dictionary

:   Displays the content of the dictionary on the screen

encrypt [file_name]

:   Encrypts the content of the specified file

send-file [file_name]

:   Sends the specified file to the server

exit

:   Closes the connection with the server (if connected) and terminates
    the program

FILES
=====

*~./src/.client.py*

:   Functional client file


BUGS
====

See GitLab Issues: <https://gitlab.csc.liv.ac.uk/sgasettl/server-client-proj/-/issues
