### Understanding Testing

Within this project there are two main tests:

- Unit tests
- Performance test

All tests can be run using `tox`. To execute, run the following:

```bash
$ tox
```

Note: The python version required to run this file is dictated in
`tox.ini`.

## Unit tests

The unit tests are using `unittest.TestCase` with Python 3. To run all the
tests within the root directory, run:

```bash
$ python3 -m unittest
```

Note: You must run using python3 or else the test will not run and fail with
an ImportError.
