#!/usr/bin/env python3
import unittest
import socket

from server import Server



class TestClientConnection(unittest.TestCase):

    def run_fake_server(self):
        # Run a server to listen for a connection and then close it
        server_sock = socket.socket()
        server_sock.bind(('127.0.0.1', 9980))
        server_sock.listen(0)
        server_sock.accept()
        server_sock.close()

    def TestClientConnectServer(self):
        start_server.server_ip = "127.0.0.1"
        start_server.port = 9980
        start_server.start_server()

        # Test the clients basic connection and disconnection
        test_client = Server.Client()
        test_client.connect('127.0.0.1', 9980)
        test_client.disconnect()

# Tests for the server connection

class TestServerConnection(unittest.TestCase):

    # Start the server in a background
    def test_server_connection(self):
        start_server = Server()
        start_server.server_ip = "127.0.0.1"
        start_server.port = 9980
        start_server.start_server()

        # Fakes a test client that attempts a connect and disconnect
        fake_client = socket.socket()
        fake_client.settimeout(0.00001)
        fake_client.connect(('127.0.0.1', 9980))
        fake_client.close()

        # Stops the server and exits
        start_server.stop_server()
