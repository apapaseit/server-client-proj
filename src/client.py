import sys
import pickle
import json
import socket
from dict2xml import dict2xml


class Client:
    """
    This class represents a client that can connect to a server,
    manipulate a dictionary and transfer files to a server
    """

    def __init__(self):
        # IP and port of the machine where a server is running
        self.server_ip = "127.0.0.1"
        self.port = 9980
        self.current_filename = ""
        # A Key-value pair data structure the user can populate
        self.dictionary = {}
        self.client_socket = None

    def connect_to_server(self):
        print('Connecting to server on %s:%d' % (self.server_ip, self.port))
        client_socket = socket.socket()
        try:
            # Trying to connect to the server
            client_socket.connect((self.server_ip, self.port))
            print('Connected.')
            return True
        except ConnectionRefusedError:  # If client unable to connect to the server
            print('Connection failed. Please make sure there is a server running on IP: %s Port: %d' % (
                self.server_ip, self.port))
            return False

    def disconnect(self):
        """
        Terminates a previously successful connection
        """
        if self.client_socket is not None:
            self.client_socket.close()

    def create_dictionary(self):
        """
        Resetting the dictionary to an empty value
        """
        self.dictionary = {}

    def add_entry(self, key, value):
        """
        Adding a new entry to the dictionary
        :param key: Key to use
        :param value: Value associated to the key
        """
        self.dictionary[key] = value

    def serialize_dictionary(self, format):
        """
        Serialize the dictionary to a file
        :param format: serialization format to use
        """
        if format == "JSON":
            with open('dico.json', 'w') as f:
                json.dump(self.dictionary, f)
                print('Dictionary serialized as JSON. Filename: dico.json')
        elif format == "BINARY":
            with open('dico.bin', 'wb') as f:
                pickle.dump(self.dictionary, f)
                print('Dictionary serialized as BINARY. Filename: dico.bin')
        elif format == "XML":
            xml_content = dict2xml(self.dictionary, wrap="dictionary", indent="  ")
            with open('dico.xml', 'w') as f:
                f.write(xml_content)
                f.close
                print('Dictionary serialized as XML. Filename: dico.xml')

    def display_dictionary(self):
        """
        Display the content of the dictionary on the screen
        :return:
        """
        print(self.dictionary)

    def send_file_to_server(self, filename):
        """
        Transfer a file to the server the client is connected to
        :param filename: Name of the file to send to the server
        """
        pass

    def encrypt_file(self, filename):
        """
        Encrypt the content of a file
        :param filename: Name of the file to use
        :return: Path to the encrypted file
        """
        pass

    def show_help_menu(self):
        """
        Display a help message on the screen
        """
        help_message = ''.join(("------------------\n",
                                "new-dictionary: Creates a new dictionary object.\n",
                                "add-entry 'key' 'value': Adds a new entry to an existing dictionary. "
                                "Automatically creates a new dictionary object if none exists.\n",
                                "serialize-dictionary [pickling-format]: Dumps the existing dictionary into a file. "
                                "Pickling format can be BINARY, JSON, or XML. Default value is JSON.\n",
                                "display-dictionary: Displays the content of the dictionary on the screen\n",
                                "encrypt [file_name]: Encrypts the content of the specified file\n",
                                "send-file [file_name]: Sends the specified file to the server.\n",
                                "exit: Closes the connection with the server (if connected) and terminates "
                                "the program."))
        print(help_message)


if __name__ == "__main__":
    client = Client()
    if client.connect_to_server():
        # display command line prompt for client to key in a command
        cmd = input("Enter a command: ")

        while cmd != "exit":
            cmd_parts = cmd.split(" ")  # Parse the command entered by the user
            command_name = cmd_parts[0]

            if command_name == "help":
                # open help menu
                client.show_help_menu()

            elif command_name == "add-entry":
                try:
                    key_name = cmd_parts[1]
                    value_string = ' '.join(cmd_parts[2:])
                    client.add_entry(key_name, value_string)
                    print("New entry added.")
                except IndexError:
                    print("Missing key:value parameters. Please enter 'help' for correct syntax")

            elif command_name == "display-dictionary":
                client.display_dictionary()

            elif command_name == "serialize-dictionary":
                if len(cmd_parts) < 2:
                    serialization_format = 'JSON'
                else:
                    serialization_format = cmd_parts[1]
                client.serialize_dictionary(serialization_format)

            cmd = input("\nEnter a command: ")

        client.disconnect()
