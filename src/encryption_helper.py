class EncryptionHelper:
    """
    Helper class that contains methods to encrypt and decrypt files
    """
    @staticmethod
    def encrypt_content(self, filename):
        """
        Encrypt the content of a file
        :param filename: Name of the file to encrypt
        :return: Path to the encrypted file
        """
        pass

    @staticmethod
    def decrypt_content(self, filename):
        """
        Decrypt the content of a file
        :param filename: name of the file to decrypt
        :return: path to the decrypted file
        """
        pass
