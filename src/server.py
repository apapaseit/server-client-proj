import configparser
import socket

from configparser import NoSectionError, NoOptionError

class Server:
    """
    This class represents the Server.
    It contains methods to bind to an IP address, listen on a specific port
    and receive files from the client.
    """

    def start_server(self):
        """
        Bind to an IP and start listening on a port in an infinite loop.
        """
        self.host_socket = socket.socket()
        self.host_socket.bind((self.server_ip, self.port))
        self.host_socket.listen()
        print('Server Started on %s port: %d' % (self.server_ip, self.port))
        # Create an infinite loop to start accepting connections from a client
        while self.running:
            client_socket, address = self.host_socket.accept()
            print(f"{address} is connected.")

    def stop_server(self):
        """
        Stopping the server
        """
        self.running = False
        self.host_socket.close()
        return

    def __load_configuration(self):
        """
        Loads configuration values from the config.cfg file
        """
        try:
            config = configparser.ConfigParser()
            config.read('config.cfg')
            # IP address to bind to
            self.server_ip = config['DEFAULT']['ServerIp']
            # Port to listen to on this host
            self.port = config['DEFAULT']['Port']
            # Should the server display the content of the received file on the screen
            self.print_to_screen = config['DEFAULT']['PrintToScreen']
            # Should the server dump the content of the received file to a local file
            self.print_to_file = config['DEFAULT']['PrintToFile']
        except (NoSectionError, NoOptionError, KeyError):
            print("Couldn't load configuration from config.cfg.")
        finally:
            # Set default values when we are unable to load from the config file
            self.server_ip = "127.0.0.1"
            self.port = 9980
            self.print_to_screen = True
            self.print_to_file = False
            self.running = True

    def __decrypt_file(self, file):
        """
        Decrypt the content of the file using the EncryptionHelper
        :param file: file to decrypt
        :return: path to the decrypted file
        """
        pass

    def __save_file(self, content, filename):
        """
        Save the content to a file
        :param content: String to dump to a file
        :param filename: Name of the file to create
        """
        pass

    def print_content(self, filename):
        """
        Display the content of the file on the screen
        :param filename: Name of the file to open and print
        """
        # Open the file in read mode only
        with open(filename, 'r') as f:
            print(f.read())

    def __init__(self):
        self.server_ip = None
        self.port = None
        self.__load_configuration()


if __name__ == "__main__":
    server = Server()
    server.start_server()
