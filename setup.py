from setuptools import setup

with open("README.md", "r") as fp:
    long_description = fp.read()

setup(
    name="server-client-proj",
    version="0.1",
    author="TDOH Inc.",
    author_email="sgasettl@liverpool.ac.uk",
    url="https://gitlab.csc.liv.ac.uk/sgasettl/server-client-proj",
    package_dir={'': 'src'},
    packages=[''],
    description="A server client network implementation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="Apache 2.0",
    classifiers=[],
    python_requires=">=3.6",
)
