# Build Server/Client Network - End Module Assignment

This project builds a simple client/server network that has the ability
to transfer a basic data set across the network.

This is a group project for the University of Liverpool's Software Development
in Practice module. The group, called TDOH Inc., is formed of the following individuals:

1. Abbey Bracken (Project Manager)
2. Alexandra Settle (Testing Engineer and Technical Documentation)
3. Alfredo Papaseit (Software Engineer and Documentation)
4. Yao Kwadzo (Software Architect and Engineer)

For more information on the practical assessment, see `assessment-details.md`.

## How do I get started?

Before you begin, ensure you install the necessary packages that can be
found in `requirements.txt`.

**Important:** Ensure you are running a minimum version of Python 3.6 for
this network to run. If you do not, this will error.

To install the requirements file, run the following command in your
shell: `pip install -r requirements.txt`

To run the network successfully, change directory to `src` on your shell
terminal and execute the following:

```bash
$ python3 server.py
```

You have now initiated your server. Open a new terminal window and execute the
following:

```bash
$ python3 client.py
```

Your client should now be connected to your server.

For helpful tips and tricks, type `help` into the client-based prompt to
discover command-line options for the client. See the `/man-pages.md` file
within `/doc/` for configuration options.

## Tests

This code runs a basic set of unit and performance tests. Please see the
unit and performance within `/test/` for more information. There is
documentation on running the tests within `/doc/testing.md`. This project
chooses to primarily use `tox` to run in an virtual environment, but the
tests can be run individually.

## Contributing

The team has defined contributing guidelines for this group project in the
`contributing.md` file in `/doc/`. Please see for more details.

Do not submit directly to the master branch under any circumstances.
Your pull request will be reverted in this case.

## License

Copyright 2021 TDOH Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
